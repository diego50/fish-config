function src
    source $HOME/.config/fish/config.fish
end

function src-edit
    nvim $HOME/.config/fish/config.fish
end

function nvim-plugins
    nvim +PlugInstall +PlugClean +PlugUpdate +UpdateRemotePlugins
end

function xterm-256
    export TERM=xterm-256color
end
alias ls exa
